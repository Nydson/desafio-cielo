import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LancamentoContaComponent } from './lancamento-conta/lancamento-conta.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'lancamento-conta', component: LancamentoContaComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomeComponent, LancamentoContaComponent]