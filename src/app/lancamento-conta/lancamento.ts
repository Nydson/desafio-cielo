export interface Lancamento {
  dataLancamentoContaCorrenteCliente?: string;
  lancamentoContaCorrenteCliente: {
    numeroRemessaBanco?: number,
    nomeSituacaoRemessa?: string,
    dadosDomicilioBancario: {
      codigoBanco?: number,
      numeroAgencia?: number,
      numeroContaCorrente?: string;
    },
    nomeTipoOperacao?: string;
  }
  dataEfetivaLancamento?: string;
  nomeBanco?: string;
  valorLancamentoRemessa?: number;
}
