import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { map } from 'rxjs/operators';
import { AppConfiguration } from '../config/app-configuration';

import { Lancamento } from './lancamento';

@Injectable({
  providedIn: 'root'
})
export class LancamentoContaService {

  constructor(private http:HttpClient, private appConfig: AppConfiguration) { }

  listaControleLancamento() {
    let baseUrl = this.appConfig.baseUrl;
    return this.http.get<Lancamento[]>(baseUrl + '/listaControleLancamento/?_sort=dataLancamentoContaCorrenteCliente&_order=asc');
  }
  
}
