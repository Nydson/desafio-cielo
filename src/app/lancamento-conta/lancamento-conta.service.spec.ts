import { TestBed } from '@angular/core/testing';

import { LancamentoContaService } from './lancamento-conta.service';

describe('LancamentoContaService', () => {
  let service: LancamentoContaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LancamentoContaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
