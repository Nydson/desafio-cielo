import { Component, OnInit } from '@angular/core';

import { Lancamento } from './lancamento';
import { LancamentoContaService } from './lancamento-conta.service';

import {
  IBarChartOptions,
  IChartistAnimationOptions,
  IChartistData
} from 'chartist';
import { ChartEvent, ChartType } from 'ng-chartist';

@Component({
  selector: 'app-lancamento-conta',
  templateUrl: './lancamento-conta.component.html',
  styleUrls: ['./lancamento-conta.component.css']
})

export class LancamentoContaComponent implements OnInit {
  lancamentos: Lancamento[] = [];
  type: ChartType = 'Bar';
  data: IChartistData = {
    labels: [],
    series: []
  };
  options: IBarChartOptions = {
    axisX: {
      showGrid: false
    },
    height: 300,
    width: 1100
  };
  events: ChartEvent = {
    draw: (data) => {
      if (data.type === 'bar') {
        data.element.animate({
          y2: <IChartistAnimationOptions>{
            dur: '0.5s',
            from: data.y1,
            to: data.y2,
            easing: 'easeOutQuad'
          }
        });
      }
    }
  };
  constructor(private lancamentoContaService:LancamentoContaService) { }
  
  ngOnInit(): void {
    this.getLancamentos();
  }

  getLancamentos(): void {   
    this.lancamentoContaService.listaControleLancamento().subscribe((response) => {
      this.lancamentos = response;

      this.setDataChart();
    });    
  }

  setDataChart(): void {
    let labels: any[] = [];
    let series: any[] = [];

    let dataChart = [];

    this.lancamentos.map(function (value, label) {
      //console.log(value.dataEfetivaLancamento, value.valorLancamentoRemessa);   

      let datLancamento = value.dataEfetivaLancamento;
      let vlrLancamento = value.valorLancamentoRemessa;

      labels.push(datLancamento);
      series.push(vlrLancamento);
    });

    this.data = 
    {
      labels: labels,
      series: [series]
    }

    // console.log(labels, series)
  }

}
