export class AppConfiguration {
    title: string;
    baseUrl: string;

    constructor() {
        this.title = '';
        this.baseUrl = '';
    }
}