## Desafio-Cielo
##### Criação de Projeto Angular com utilização das tecnologias:
* Angular: 11.2.10
* Angular CLI: 11.2.9
* Node: 10.19.0
* Json Server: 0.16.3
* Bootstrap: 4.6.0
* JQuery: 3.6.0
* Chartist: 0.11.4
* Cucumber: 6.0.5

### Instalando a Aplicação
* npm install

### Rodando a Aplicação (Angular)
* npm run start

##### A aplicação estará funcionando no link:
* http://localhost:4200

### Rodando a Aplicação API (Json Server)
* npm run mock:server

##### Endpoints retornados:
* http://localhost:3000/totalControleLancamento
* http://localhost:3000/listaControleLancamento
* http://localhost:3000/paginacaoControleLancamento

##### Home:
* http://localhost:3000

### Rodando Aplicação (Angular + Json Server)
* npm run start:proxy:mock:server

##### A aplicação estará funcionando no link:
* http://localhost:4200
##### Apontando para aplicação API no link:
* http://localhost:3000

### Rodando Testes Automatizados
* npm run e2e

##### Cenário de teste conforme configuração do arquivo: ./e2e/src/features/app.feature
* Vá para a Home e (Mostre o título)
